public class CodingTest1 {
    public int solution(int N) {
        int[] sumArr = new int[N];
        F(N, sumArr);

        int largestVal = N - sumArr[0];
        int minIdx = 0;
        for (int i = 1; i < sumArr.length; i++) {
            int val = N - sumArr[i];
            if (val >= 0) {
                if (val < largestVal) {
                    largestVal = val;
                    minIdx = i + 1;
                }
            }
        }
        System.out.println(sumArr[minIdx - 1]);
        return minIdx;
    }

    public int F(int K, int[] sumArr) {
        if(K <= 1) return 1;

        if (sumArr[K - 1] != 0) {
            return sumArr[K - 1];
        }

        sumArr[K - 1] = F(K - 1, sumArr) + K;

        return sumArr[K - 1];
    }

    public static void main(String[] args) {
        CodingTest1 codingTest1 = new CodingTest1();
        System.out.println(codingTest1.solution(5633));
    }
}
