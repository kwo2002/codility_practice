public class ArrMultiply {
    public static int[] multiply(int[] input) {
        if (input == null || input.length == 0) {
            throw new IllegalArgumentException();
        }
        int N = input.length;

        int[] one = new int[N];
        int product = 1;

        for(int i = 0; i < N; i++) {
            one[i] = product;
            product *= input[i];
        }

        product = 1;
        int[] two = new int[N];
        for(int i = N-1; i >= 0; i--) {
            two[i] = product;
            product *= input[i];
        }

        int[] output = new int[N];
        for (int i = 0; i < N; i++) {
            output[i] = one[i] * two[i];
        }
        return output;
    }

    public static void main(String[] args) {
        int[] multiply = multiply(new int[]{1, 2, 3, 4, 5});
        for (int i : multiply) {
            System.out.println(i);
        }
    }
}
