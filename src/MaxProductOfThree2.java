import java.util.Arrays;

public class MaxProductOfThree2 {
    public int solution(int[] A) {
        Arrays.sort(A);

        int product1 = A[0] * A[1] * A[A.length - 1];
        int product2 = A[A.length - 3] * A[A.length - 2] * A[A.length - 1];
        return product1 > product2 ? product1 : product2;
    }
    public static void main(String[] args) {
        MaxProductOfThree2 maxProductOfThree2 = new MaxProductOfThree2();
        System.out.println(maxProductOfThree2.solution(new int[]{-3, 1, 2, -2, 5, 6}));
    }
}
