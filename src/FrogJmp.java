public class FrogJmp {
    public int solution(int X, int Y, int D) {
        int distance = Y - X;

        int jumpCnt = distance / D;
        if (distance % D >= 1) {
            jumpCnt += 1;
        }

        return jumpCnt;
    }

    public static void main(String[] args) {
        FrogJmp frogJmp = new FrogJmp();
        System.out.println(frogJmp.solution(10, 85, 30));
    }
}
