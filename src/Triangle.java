public class Triangle {
    public int solution(int[] A) {

        for (int i = 0; i < A.length - 2; i++) {
            int p = A[i];
            for (int j = i + 1; j < A.length - 1; j++) {
                int q = A[j];
                for (int k = j + 1; k < A.length; k++) {
                    int r = A[k];
                    if(p + q > r && q + r > p && r + p > q) return 1;
                }
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        Triangle triangle = new Triangle();
        System.out.println(triangle.solution(new int[]{10, 2, 5, 1}));
    }
}
