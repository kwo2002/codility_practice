import java.util.Arrays;

public class Triangle2 {
    public int solution(int[] A) {
        Arrays.sort(A);
        for (int i = 2; i < A.length; i++) {
            if (A[i - 2] + A[i - 1] > A[i]) {
                return 1;
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        Triangle2 triangle2 = new Triangle2();
        System.out.println(triangle2.solution(new int[]{10, 2, 5, 1, 8, 20}));
    }
}
