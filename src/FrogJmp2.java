public class FrogJmp2 {
    public int solution(int X, int Y, int D) {
        int distance = Y - X;
        int mod = distance % D;

        if(mod > 0) return (distance / D) + 1;
        else return (distance / D);
    }

    public static void main(String[] args) {
        FrogJmp2 frogJmp2 = new FrogJmp2();
        System.out.println(frogJmp2.solution(10, 85, 10));
    }
}
