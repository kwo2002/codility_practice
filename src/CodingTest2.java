import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CodingTest2 {
    public int solution(int[] A) {
        if(A.length == 1) return A[0] + A[0];
        if (A.length == 2) {
            int first = A[0];
            int second = A[1];

            if(first >= second) return A[0] + A[0];
            else return A[1] + A[1];
        }

        Map<Integer, Integer> distanceMap = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            distanceMap.put(A[i], i);
        }

        Arrays.sort(A);
        int appeal = 0;

        int sum = 0;
        int result = A[0];

        for (int i = 1; i < A.length; i++) {
            int val = A[i];
            int max = A[i - 1] + A[i];

            sum = Math.max(val, max);
            result = Math.max(sum, result);
        }

        for (int i = A.length; i > A.length - 1; i--) {
            int sameCityAppeal = (A[i - 1] + A[i - 1]);

            int distance = Math.abs(distanceMap.get(A[i - 1]) - distanceMap.get(A[i - 2]));
            int otherCityAppeal = (A[A.length - 1] + A[i - 1]) + distance;

            int max = Math.max(sameCityAppeal, otherCityAppeal);
            appeal = Math.max(appeal, max);
        }

        return appeal;
    }

    public static void main(String[] args) {
        CodingTest2 codingTest2 = new CodingTest2();
        System.out.println(codingTest2.solution(new int[]{1, 1, 1, 1, 1, 1}));
    }
}
