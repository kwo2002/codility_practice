import java.util.Arrays;

public class MyArrayList {
    Object[] v;
    int currIdx = 0;

    public MyArrayList() {
        v = new Object[10];
    }

    public <T> void add(T t) {
        checkAvailableSize();
        v[currIdx] = t;
        currIdx++;
    }

    public Object get(int idx) {
        if(idx > currIdx) throw new IllegalArgumentException();
        return v[idx];
    }

    public int size() {
        return currIdx;
    }

    public int length() {
        return v.length;
    }

    // 남은 배열 사이즈가 얼마 없다면 배열의 크기를 키운다.
    private void checkAvailableSize() {
        if (currIdx == v.length / 2) {
            int size = v.length + currIdx;
            v = Arrays.copyOf(v, size);
        }
    }
}
