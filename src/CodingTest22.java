public class CodingTest22 {
    public int solution(int[] A) {
        if(A.length == 1) return A[0] + A[0];
        if (A.length == 2) {
            int first = A[0];
            int second = A[1];

            if(first >= second) return A[0] + A[0];
            else return A[1] + A[1];
        }

        int maxAppeal = 0;
        int maxIdx = 0;
        for (int i = 0; i < A.length; i++) {
            if(A[i] > maxAppeal){
                maxAppeal = A[i];
                maxIdx = i;
            }
        }

        int appeal = 0;
        for (int i = 0; i < A.length; i++) {
            int val = (A[i] + maxAppeal) + (Math.abs(maxIdx - i));
            if (appeal < val) {
                appeal = val;
            }
        }

        return appeal;
    }

    public static void main(String[] args) {
        CodingTest22 codingTest22 = new CodingTest22();
        System.out.println(codingTest22.solution(new int[]{ -8,4,0,5,-3,6}));
    }
}
