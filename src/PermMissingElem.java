public class PermMissingElem {
    public int solution(int[] A) {
        int n = A.length + 1;
        int totalSum = n * (n + 1) / 2;

        for (int i = 0; i < A.length; i++) {
            totalSum -= A[i];
        }

        return totalSum;
    }

    public static void main(String[] args) {
        PermMissingElem permMissingElem = new PermMissingElem();
        System.out.println(permMissingElem.solution(new int[]{2, 3, 1, 5, 4, 6, 7, 8, 9, 10, 12}));
    }
}
