import java.util.Arrays;

public class MaxProductOfThree {
    public int solution(int[] A) {
        /*int temp[] = new int[1000];
        for (int i = 0; i < A.length; i++) {
            temp[A[i]] = i;
        }*/
// [-5, -5, 4, 5]
        if(A.length == 3) return A[0] * A[1] * A[2];
        Arrays.sort(A);
        int maxProduct = Integer.MIN_VALUE;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (maxProduct < A[i] * A[A.length - 1 - j] * A[A.length - 1 - (j + 1)]) {
                    maxProduct = A[i] * A[A.length - 1 - j] * A[A.length - 1 - (j + 1)];
                }
            }
        }

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                if (maxProduct < A[i] * A[i+1] * A[A.length - 1 - j]) {
                    maxProduct = A[i] * A[i+1] * A[A.length - 1 - j];
                }
            }
        }

        int prd = A[A.length - 1] * A[A.length - 2] * A[A.length - 3];
        return prd > maxProduct ? prd : maxProduct;
    }

    public static void main(String[] args) {
        MaxProductOfThree maxProductOfThree = new MaxProductOfThree();
        System.out.println(maxProductOfThree.solution(new int[]{-10, -2, -4}));
//        System.out.println(maxProductOfThree.solution(ArrayUtil.generateArr(100000, -1000, 1000)));
    }
}
