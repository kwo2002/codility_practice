import java.util.*;

import static java.util.stream.Collectors.toList;

public class Acmicpc7785 {
    public List<String> solution(List<String> list) {
        Map<String, Integer> map = new HashMap<>();

        for (String s : list) {
            map.put(s, map.getOrDefault(s, 0) + 1);
        }

        List<String> result = new ArrayList<>();
        for (String s : map.keySet()) {
            if(map.get(s) % 2 == 1) result.add(s);
        }

        return result.stream().sorted().collect(toList());
    }

    public static void main(String[] args) {
        Acmicpc7785 acmicpc7785 = new Acmicpc7785();
        List<String> solution = acmicpc7785.solution(Arrays.asList("Baha", "Askar", "Baha", "Artem"));
        for (String s : solution) {
            System.out.println(s);
        }
    }
}
