// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class DistinctArray {
    public int solution(int[] A) {
//        return (int)Arrays.stream(A).distinct().count();
        boolean[] flag = new boolean[A.length];

        for (int i = 0; i < A.length; i++) {
            flag[A[i]] = true;
        }

        int cnt = 0;
        for (int i = 0; i < A.length; i++) {
            if(flag[i]) cnt++;
        }
        return cnt;
    }

    public static void main(String[] args) {
        DistinctArray distinctArray = new DistinctArray();
        System.out.println(distinctArray.solution(new int[]{2, 1, 1, 2, 3, 1}));
    }
}