import java.util.Arrays;

public class PermCheck1 {
    public int solution(int[] A) {
        if(A.length == 1 && A[0] == 1)
        Arrays.sort(A);
        int val = A[0];
        if(val != 1) return 0;
        boolean flag = false;
        for (int i = 1; i < A.length; i++) {
            if (A[i] != val) {
                if (A[i] != val + 1) {
                    return 0;
                } else {
                    flag = true;
                }
            }

            val = A[i];
        }

        return flag ? 1 : 0;
    }

    public static void main(String[] args) {
        PermCheck1 permCheck1 = new PermCheck1();
        System.out.println(permCheck1.solution(new int[]{4, 1, 3, 2}));
    }
}
