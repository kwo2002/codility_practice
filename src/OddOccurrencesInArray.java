import java.util.Arrays;
import java.util.Stack;

public class OddOccurrencesInArray {
    public int solution(int[] A) {
        Arrays.sort(A);
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < A.length; i++) {
            if (!stack.isEmpty()) {
                if (stack.peek() == A[i]) stack.pop();
            } else {
                stack.push(A[i]);
            }
        }
        return stack.pop();
    }

    public static void main(String[] args) {
        OddOccurrencesInArray oddOccurrencesInArray = new OddOccurrencesInArray();
        System.out.println(oddOccurrencesInArray.solution(new int[]{9, 3, 9, 3, 9, 7, 9}));
    }
}
