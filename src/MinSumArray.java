public class MinSumArray {

    public int solution(int[] A) {
        if(A == null || A.length <= 0) {
            throw new IllegalArgumentException();
        }
        int N = A.length;
        if(N < 2 || N > 100000) {
            throw new IllegalArgumentException();
        }

        int[] sumArr = new int[N];
        int sum = 0;
        for(int i = 0; i < N; i++) {
            if(A[i] < -1000 || A[i] > 1000) throw new IllegalArgumentException();

            sum+=A[i];
            sumArr[i] = sum;
        }

        int minSum = -1;
        for(int i = 0; i < N - 1; i++) {
            int calc = sumArr[i] - (sumArr[N-1] - sumArr[i]);
            if(calc < 0) calc*=-1;

            if(minSum == -1) minSum = calc;
            if(calc < minSum) minSum = calc;
        }

        return minSum;
    }

    public static void main(String[] args) {
        MinSumArray minSumArray = new MinSumArray();
        System.out.println(minSumArray.solution(new int[]{3, 1}));
    }
}
