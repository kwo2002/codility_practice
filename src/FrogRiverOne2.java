import java.util.HashMap;
import java.util.Map;

public class FrogRiverOne2 {
    public int solution(int X, int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            if(A[i] <= X) map.put(A[i], i);
            if(map.keySet().size() == X) return i;
        }

        return -1;
    }

    public static void main(String[] args) {
        FrogRiverOne2 frogRiverOne2 = new FrogRiverOne2();
        System.out.println(frogRiverOne2.solution(5, new int[]{1, 3, 1, 4, 2, 3, 5, 4}));
    }
}
