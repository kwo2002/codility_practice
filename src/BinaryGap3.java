public class BinaryGap3 {
    public int solution(int N) {
        String binaryN = Integer.toBinaryString(N);

        boolean oneFlag = false;

        int zeroCnt = 0;
        int maxZeroCnt = 0;
        for (int i = 0; i < binaryN.length(); i++) {
            if (binaryN.charAt(i) == '1') {
                if (!oneFlag) {
                    oneFlag = true;
                } else {
                    if (maxZeroCnt < zeroCnt) {
                        maxZeroCnt = zeroCnt;
                    }
                    zeroCnt = 0;
                }
            } else {
                zeroCnt++;
            }
        }
        return maxZeroCnt;
    }

    public static void main(String[] args) {
        BinaryGap3 binaryGap3 = new BinaryGap3();
        System.out.println(binaryGap3.solution(561892));
    }

}
