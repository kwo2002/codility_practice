public class SelectionSort {
    public void sort(int[] A) {
        for (int i = 0; i < A.length - 1; i++) {
            int minIdx = 0;
            for (int j = i + 1; j < A.length; j++) {
                if (A[j] < A[minIdx]) {
                    minIdx = j;
                }
            }
            swap(A, i, minIdx);
        }

        for (int i : A) {
            System.out.println(i);
        }
    }

    public void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static void main(String[] args) {
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.sort(new int[]{3, 1, 5, 2, 4});
    }
}

