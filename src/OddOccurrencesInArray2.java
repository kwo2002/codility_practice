import java.util.HashMap;
import java.util.Map;

public class OddOccurrencesInArray2 {
    public int solution(int[] A) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < A.length; i++) {
            map.put(A[i], map.getOrDefault(A[i], 0) + 1);
        }

        for (int i = 0; i < A.length; i++) {
            if (map.get(A[i]) % 2 == 1) {
                return A[i];
            }
        }

        return -1;
    }

    public static void main(String[] args) {
        OddOccurrencesInArray2 oddOccurrencesInArray2 = new OddOccurrencesInArray2();
        System.out.println(oddOccurrencesInArray2.solution(new int[]{9, 3, 9, 3, 9, 7, 9}));

    }
}
