public class MinAvgTwoSlice2 {
    public int solution(int[] A) {
        float minAvg = Integer.MAX_VALUE;
        int minIdx = 0;
        for (int i = 0; i < A.length; i++) {
            float sum = A[i];
            for (int j = i + 1; j < A.length; j++) {
                sum += A[j];
                float avg = sum / (j - i + 1);
                if (minAvg > avg) {
                    minAvg = avg;
                    minIdx = i;
                }
            }
        }

        return minIdx;
    }

    public static void main(String[] args) {
        MinAvgTwoSlice2 minAvgTwoSlice2 = new MinAvgTwoSlice2();
        System.out.println(minAvgTwoSlice2.solution(new int[]{4, 2, 2, 5, 1, 5, 8}));

    }
}
