public class DynamicFibonacci {
    private int[] memo = new int[1000];

    public int fib(int n) {
        if(n <= 1) return n;

        if(memo[n] != 0) return memo[n];
        memo[n] = fib(n - 1) + fib(n - 2);
        return memo[n];
    }

    public static void main(String[] args) {
        DynamicFibonacci dynamicFibonacci = new DynamicFibonacci();
        int result = dynamicFibonacci.fib(40);
        System.out.println(result);
    }
}
