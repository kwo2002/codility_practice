public class CodingTest2222 {
    public int solution(int[] A) {
        int firstCityIdx = 0;
        int secondCityIdx = 0;

        int firstCity = 0;
        int ret = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] - i > firstCity) {
                firstCity = A[i] - i;
                firstCityIdx = i;
            }

            if (firstCity + A[i] + Math.abs(firstCityIdx - i) > ret) {
                ret = firstCity + A[i];
                secondCityIdx = i;
            }

            ret = Math.max(A[i] + A[i], ret);

        }

        System.out.println("firstCityIndex: " +firstCityIdx);
        System.out.println("secondCityIndex: " +secondCityIdx);
        int distance = Math.abs(firstCityIdx - secondCityIdx);
        return A[firstCityIdx
                ] + A[secondCityIdx] + distance;
    }

    public static void main(String[] args) {
        CodingTest2222 codingTest2222 = new CodingTest2222();
//        System.out.println(codingTest2222.solution(new int[]{-8, 4, 0, 5, -3, 6}));

        System.out.println(codingTest2222.solution(ArrayUtil.generateArr(20, -10, 20)));
//        System.out.println(codingTest2222.solution(new int[]{2, 4, 1, -6, 14, 1, 18, 1, -10, -9, -3, -10, 14, -1, 11, 10, 18}));

    }
}
