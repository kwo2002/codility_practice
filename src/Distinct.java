import java.util.HashSet;
import java.util.Set;

public class Distinct {
    public int solution(int[] A) {
        if(A.length > 100000) throw new IllegalArgumentException();
        Set<Integer> el = new HashSet<>();

        for (int i = 0; i < A.length; i++) {
            el.add(A[i]);
        }

        return el.size();
    }
    public static void main(String[] args) {
        Distinct distinct = new Distinct();
        System.out.println(distinct.solution(new int[]{2, 1, 1, 2, 3, 1}));
    }
}
