public class CyclicRotation {
    public int[] solution(int[] A, int K) {
        for (int i = 0; i < K; i++) {
            for (int j = 0; j < A.length; j++) {
                int temp = A[0];
                A[0] = A[j];
                A[j] = temp;
            }
        }
        return A;
    }

    public static void main(String[] args) {
        CyclicRotation cyclicRotation = new CyclicRotation();
        int[] a = {3, 8, 9, 7, 6};
        int[] solution = cyclicRotation.solution(a, 3);
        for (int i : solution) {
            System.out.println(i);
        }


    }
}
