public class MaxProfit2 {
    public int solution(int[] A) {
        int minPrice= A[0];
        int profit = 0;
        int ret = 0;

        for (int i = 1; i < A.length; i++) {
            minPrice = Math.min(minPrice, A[i]);
            profit = Math.max(profit, A[i] - minPrice);

            ret = Math.max(profit, ret);
        }

        return ret;
    }

    public static void main(String[] args) {
        MaxProfit2 maxProfit2 = new MaxProfit2();
        System.out.println(maxProfit2.solution(new int[]{23171, 21011, 21123, 21366, 21013, 21367}));
    }
}
