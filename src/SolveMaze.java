public class SolveMaze {

    public boolean solve(int[][] maze, int y, int x) {
        int WALL = 0;
        int WAY = 1;

        if (maze == null || maze.length <= 0) {
            throw new IllegalArgumentException();
        }

        if (maze[y][x] == WALL) {
            return false;
        }

        if (x == maze[0].length - 1 || !solve(maze, y, x + 1)) {
            if(y == maze.length - 1) return false;
            solve(maze, y + 1, x);
        }

        return true;
    }

    public static void main(String[] args) {
        SolveMaze solveMaze = new SolveMaze();
        int[][] maze = new int[][]{
                {1, 0, 0, 0},
                {1, 1, 1, 1},
                {1, 0, 0, 1},
                {3, 0, 0, 0}
        };

        System.out.println(solveMaze.solve(maze, 0, 0));

    }
}
