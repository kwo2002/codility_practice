public class MinAvg {
    public int solution(int[] A) {
        if(A.length == 2) return (A[0] + A[1]) / 2;

        int minAvg = Integer.MAX_VALUE;
        for (int i = 0; i < A.length; i++) {
            int sum = A[i];
            for (int j = i + 1; j < A.length; j++) {
                sum += A[j];
                int avg = sum / (j - i + 1);

                if(avg < minAvg) minAvg = avg;
            }
        }
        return minAvg;
    }

    public int solution2(int[] A) {


        int psum = A[0];
        int ret = 0;
        for (int i = 1; i < A.length; i++) {
            psum = Math.min(A[i] + psum, A[i]);
        }
        return 0;
    }

    public static void main(String[] args) {
        MinAvg minAvg = new MinAvg();
        System.out.println(minAvg.solution(new int[]{4, 2, 2, 5, 1, 5, 8}));
    }
}
