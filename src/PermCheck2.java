import java.util.Arrays;

public class PermCheck2 {
    public int solution(int[] A) {

        if(A.length == 1) {
            if(A[0] == 1) return 1;
            return 0;
        }

        if(A == null || A.length <= 0) return 0;

        Arrays.sort(A);
        if(A[0] != 1) return 0;
        for (int i = 0; i < A.length - 1; i++) {
            if (A[i] + 1 != A[i + 1]) {
                return 0;
            }
        }

        return 1;
    }

    public static void main(String[] args) {
        PermCheck2 permCheck2 = new PermCheck2();
        System.out.println(permCheck2.solution(new int[]{4, 1, 1, 3, 2}));
    }
}
