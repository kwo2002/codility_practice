import java.util.concurrent.ThreadLocalRandom;

public class MaxCounters2 {
    public int[] solution(int N, int[] A) {
        int[] arr = new int[N];

        int maxVal = 0;
        int maxCounter = 0;
        for (int i = 0; i < A.length; i++) {
            int val = A[i];
            if (val <= N) {
                int inc = ++arr[val - 1];
                if(inc + maxCounter > maxVal) maxVal = inc + maxCounter;
            } else if (val == N + 1) {
                maxCounter = maxVal;
                arr = new int[N];
            }
        }

        for (int i = 0; i < N; i++) {
            arr[i] += maxCounter;
        }

        return arr;
    }

    public static int[] generateArr(int size, int range) {
        int n = ThreadLocalRandom.current().nextInt(1, size);
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(1, range + 1);
        }

        return arr;
    }

    public static void main(String[] args) {
        MaxCounters2 maxCounters2 = new MaxCounters2();
        int[] solution = maxCounters2.solution(5, generateArr(15, 6));
        for (int i : solution) {
            System.out.println(i);
        }

    }
}
