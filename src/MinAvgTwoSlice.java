public class MinAvgTwoSlice {
    public int solution(int[] A) {
        int minIndex = 0;
        float minAvg = 9999f;
        float sum = 0;

        for (int i = 0; i < A.length; i++) {
            sum += A[i];
            for (int j = i + 1; j < A.length; j++) {
                sum += A[j];
                int qp = j - i + 1;
                float avg = sum / qp;
                if (avg < minAvg) {
                    minAvg = avg;
                    minIndex = i;
                }
            }
            sum = 0;
        }
        return minIndex;
    }

    public int solution2(int[] A) {
        float sum = A[0] + A[1];
        float minAvg = sum / 2;
        int minIdx = 0;

        for (int i = 2; i < A.length; i++) {
            int cnt = 0;
            sum += A[i];
            float avg = sum / (i + 1);
            if (avg < minAvg) minAvg = avg;

            float tempSum = sum;
            while (i - 1 > cnt) {
                tempSum -= A[cnt++];
                float avg2 = tempSum / (i + 1 - cnt);
                if (avg2 < minAvg) {
                    minAvg = avg2;
                    minIdx = cnt;
                }
            }
        }
        return minIdx;
    }

    public static void main(String[] args) {
        MinAvgTwoSlice minAvgTwoSlice = new MinAvgTwoSlice();
        int minAvg = minAvgTwoSlice.solution2(new int[]{5, 6, 3, 4, 9});
        System.out.println(minAvg);
    }
}
