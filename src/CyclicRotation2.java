public class CyclicRotation2 {
    public int[] solution(int[] A, int K) {
        if(A.length == K) return A;
        for (int i = 0; i < K; i++) {

            for (int j = 1; j < A.length; j++) {
                swap(A, 0, j);
            }

        }
        return A;
    }

    public void swap(int[] A, int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public static void main(String[] args) {
        CyclicRotation2 cyclicRotation2 = new CyclicRotation2();
        int[] solution = cyclicRotation2.solution(new int[]{3, 8, 9, 7, 6}, 3);
        for (int i : solution) {
            System.out.println(i);
        }
    }
}
