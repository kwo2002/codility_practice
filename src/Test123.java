public class Test123 {
    public int solution(int[] A) {

        boolean[] flag = new boolean[A.length + 1];

        for (int i = 0; i < A.length; i++) {
            int val = A[i];
            if (val > 0 && val < flag.length) {
                flag[val] = true;
            }
        }

        for (int i = 1; i < flag.length; i++) {
            if (!flag[i]) {
                return i;
            }
        }
        return flag.length;
    }

    public static void main(String[] args) {

    }
}
