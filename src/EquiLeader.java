import java.util.HashMap;
import java.util.Map;

public class EquiLeader {
    public int solution(int[] A) {
        int leader = findLeader1(A);

        int totalLeader = 0;
        for (int i : A) {
            if(i == leader) totalLeader++;
        }

        int equiLeaderCnt = 0;
        int leftLeaderCnt = 0;
        for (int i = 0; i < A.length; i++) {
            if(A[i] == leader) leftLeaderCnt++;
            int rightLeaderCnt = totalLeader - leftLeaderCnt;

            if (leftLeaderCnt > (i + 1) / 2 && rightLeaderCnt > (A.length - i - 1) / 2) {
                equiLeaderCnt++;
            }
        }

        return equiLeaderCnt;
    }

    public int findLeader(int[] A) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < A.length; i++) {
            map.put(A[i], map.getOrDefault(A[i], 0) + 1);
        }

        int maxOccurCnt = 0;
        int leader = -1;
        for (Integer k : map.keySet()) {
            Integer occurCnt = map.get(k);
            if(occurCnt > maxOccurCnt) {
                maxOccurCnt = occurCnt;
                leader = k;
            }
        }

        return leader;
    }

    public int findLeader1(int[] A) {
        int leader = A[0];
        int cnt = 1;
        for (int i = 1; i < A.length; i++) {
            if (leader == A[i]) {
                cnt++;
            } else {
                cnt--;
            }

            if (cnt == 0) {
                cnt = 1;
                leader = A[i];
            }
        }

        return leader;
    }


    public static void main(String[] args) {
        EquiLeader equiLeader = new EquiLeader();
        System.out.println(equiLeader.solution(new int[]{4, 3, 4, 4, 4, 2}));
    }
}
