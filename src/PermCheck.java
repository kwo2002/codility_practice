import java.util.Arrays;

public class PermCheck {
    public int solution(int[] A) {
        boolean zeroFlag = false;
        for (int i=0; i< A.length; i++) {
            if(A[i] > A.length) return 0;
            if(A[i] == 1) zeroFlag = true;
        }
        Arrays.sort(A);
        if(!zeroFlag) return 0;
        return 1;
    }

    public static void main(String[] args) {
        PermCheck permCheck = new PermCheck();
        System.out.println(permCheck.solution(new int[]{2, 3}));
    }
}
