import java.util.*;

public class Codility1 {
    public int solution(int[] A) {
        if(A.length <= 0 || A == null) {
            throw new IllegalArgumentException();
        }

        int len = A.length;
        int mod = len % 2;

        if(mod == 0) {
            throw new IllegalArgumentException();
        }
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            if (list.contains(A[i])) {
                Integer a = A[i];
                list.remove(a);

            } else {
                list.add(A[i]);
            }
        }

        if(list.size() ==1) return list.get(0);

        return -1;
    }

    public static void main(String[] args) {
        Codility1 codility1 = new Codility1();
        System.out.println(codility1.solution(new int[]{9, 3, 9, 3, 9, 7, 9}));
    }
}
