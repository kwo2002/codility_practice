public class PassingCars {
    public int solution(int[] A) {
        int east = 0;
        int pair = 0;
        for (int i = 0; i < A.length; i++) {
            if (A[i] == 0) {
                east++;
            } else {
                pair += east;
            }
        }

        if(pair > 1_000_000_000) return -1;
        return pair;
    }

    public static void main(String[] args) {
        PassingCars passingCars = new PassingCars();
        System.out.println(passingCars.solution(new int[]{0, 1, 0, 1, 1}));
    }
}
