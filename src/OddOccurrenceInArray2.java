import java.util.Arrays;

public class OddOccurrenceInArray2 {
    public int solution(int[] A) {
        if(A.length == 1) return A[0];
        Arrays.sort(A);
        int cnt = 1;
        int prev = A[0];

        for (int i = 1; i < A.length; i++) {
            if(A[i] == prev) cnt++;
            else {
                if(cnt % 2 != 0) {
                    return prev;
                }
                cnt = 1;
            }

            prev = A[i];
        }

        return A[A.length - 1];
    }

    public static void main(String[] args) {
        OddOccurrenceInArray2 oddOccurrenceInArray2 = new OddOccurrenceInArray2();
        System.out.println(oddOccurrenceInArray2.solution(new int[]{2, 2, 3, 3, 4}));
    }
}
