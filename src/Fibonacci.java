public class Fibonacci {
    public long fib(int n) {
        if(n == 0) return 0;
        if(n == 1) return 1;

        long begin = 0;
        long end = 1;
        for (int i = 0; i <= n; i++) {

            if (i > 1) {
                System.out.print((begin + end) + " ");
                long  temp = begin;
                begin = end;
                end = temp + end;
            } else
                System.out.print(i + " ");
        }
        return 0;
    }

    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();
        fibonacci.fib(100);
    }
}
