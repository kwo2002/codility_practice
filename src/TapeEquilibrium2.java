public class TapeEquilibrium2 {
    public int solution(int[] A) {
        int totalSum = 0;
        for (int i = 0; i < A.length; i++) {
            totalSum += A[i];
        }
        int minDiff = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < A.length - 1; i++) {
            sum += A[i];
            int diff = Math.abs(sum - (totalSum - sum));
            if (diff < minDiff) minDiff = diff;
        }
        return minDiff;
    }

    public static void main(String[] args) {
        TapeEquilibrium2 tapeEquilibrium2 = new TapeEquilibrium2();
        System.out.println(tapeEquilibrium2.solution(new int[]{-1000, 1000}));
    }
}
