import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Leader {
    public int solution(int[] A) {
        int[] arr = Arrays.copyOf(A, A.length);
        int leader = findLeader(arr);

        int equiLeaderCnt = 0;

        int leftLeaderCnt = 0;
        int leftOtherCnt = 0;
        for (int i = 0; i < A.length; i++) {
            if(A[i] == leader) leftLeaderCnt++;
            else leftOtherCnt++;

            if (leftLeaderCnt > leftOtherCnt) {
                int rightLeaderCnt = 0;
                int rightOtherCnt = 0;
                for (int j = i + 1; j < A.length; j++) {
                    if(A[j] == leader) rightLeaderCnt++;
                    else rightOtherCnt++;
                }

                if(rightLeaderCnt > rightOtherCnt) equiLeaderCnt++;
            }
        }
        return equiLeaderCnt;
    }

    public int solution2(int[] A) {
        int[] arr = Arrays.copyOf(A, A.length);
        int leader = findLeader(arr);

        Map<Integer, Integer> map = new HashMap<>();
        int cnt = 0;
        for (int i = 0; i < A.length; i++) {
            if(A[i] == leader) cnt++;
            map.put(i, cnt);
        }

        int result = 0;
        for (Integer k : map.keySet()) {
            Integer leaderCnt = map.get(k);
            int otherCnt = (k + 1) - leaderCnt;

            if (leaderCnt > otherCnt) {
                Integer lastLeaderCnt = map.get(A.length - 1) - leaderCnt;
                int lastOtherCnt = (A.length - k - 1) - lastLeaderCnt;
                if(lastLeaderCnt > lastOtherCnt) result++;
            }

        }
        return result;
    }


    public int findLeader(int[] arr) {
        Arrays.sort(arr);
        int leader = -1;

        int maxCnt = 0;
        int cnt = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == arr[i - 1]) {
                cnt++;
                if(cnt > maxCnt) {
                    maxCnt = cnt;
                    leader = arr[i];
                }
            } else {
                cnt = 1;
            }
        }
        return leader;
    }

    public static void main(String[] args) {
        Leader leader = new Leader();
        System.out.println(leader.solution2(new int[]{4, 3, 4, 4, 4, 2}));
    }
}
