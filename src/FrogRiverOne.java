public class FrogRiverOne {
    public int solution(int X, int[] A) {
        int N = A.length;
        boolean[] checkArr = new boolean[X];

        int cnt = 0;
        for(int i = 0; i < N; i++) {
            if(A[i] <= X) {
                if(!checkArr[A[i] - 1]) cnt++;
                checkArr[A[i] - 1] = true;
            }

            if(cnt == X) return i;
                /*boolean flag = true;
                for(int j = 0; j < X; j++) {
                    if(!checkArr[j]) flag = false;
                }

                if(flag) return i;
            }*/
        }

        return -1;
    }

    public static void main(String[] args) {
        FrogRiverOne frogRiverOne = new FrogRiverOne();
        System.out.println(frogRiverOne.solution(3, new int[]{1, 3, 1, 3, 2, 1, 3}));
    }
}
