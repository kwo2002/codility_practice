public class CodingTest222 {
    public int solution(int[] A) {
        if(A.length == 1) return A[0] + A[0];
        if (A.length == 2) {
            int first = A[0];
            int second = A[1];

            if(first >= second) return A[0] + A[0];
            else return A[1] + A[1];
        }

        int maxAppeal = 0;
        for (int i = 0; i < A.length; i++) {
            int firstCity = A[i];
            for (int j = i; j < A.length; j++) {
                int secondCity = A[j];
                int val = (firstCity + secondCity) + (Math.abs(j - i));
                if(val > maxAppeal) maxAppeal = val;
            }
        }

        return maxAppeal;
    }
    public static void main(String[] args) {
        CodingTest222 codingTest222 = new CodingTest222();
        System.out.println(codingTest222.solution(ArrayUtil.generateArr(20, -10, 20)));

    }
}
