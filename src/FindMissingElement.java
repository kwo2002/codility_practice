import java.util.Arrays;

public class FindMissingElement {
    public int solution(int[] A) {
        if(A == null || A.length <= 0) return 1;
        if(A.length > 100000) throw new IllegalArgumentException();
        Arrays.sort(A);
        if(A[0] > 1) return 1;
        if (A.length == 1) {
            if(A[0] > 1) return 1;
            if(A[0] == 1) return 2;
            if(A[0] < 1) return 1;
        }

        int previousElement = 0;

        for (int i : A) {
            if (i == previousElement + 2) {
                return previousElement + 1;
            }
            previousElement = i;
        }

        return previousElement + 1;
    }

    public static void main(String[] args) {
        FindMissingElement findMissingElement = new FindMissingElement();
        System.out.println(findMissingElement.solution(new int[]{1, 2, 3, 4, 5}));
    }
}
