import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class MissingInteger2 {
    public int solution(int[] A) {
        boolean[] flag = new boolean[A.length + 1];

        for (int i = 0; i < A.length; i++) {
            int val = A[i];
            if (val > 0 && val < A.length) {
                flag[val] = true;
            }
        }

        for (int i = 1; i < flag.length; i++) {
            if(!flag[i]) return i;
        }

        return flag.length;
    }

    public static int[] generateArr(int size, int startRange, int endRange) {
        int n = ThreadLocalRandom.current().nextInt(1, size);
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(startRange, endRange);
        }

        return arr;
    }

    public static void main(String[] args) {
        MissingInteger2 missingInteger2 = new MissingInteger2();
        System.out.println(missingInteger2.solution(new int[]{-99999, 1, 2, 3, 4, 5, 99999}));
    }
}
