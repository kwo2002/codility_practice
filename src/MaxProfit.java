public class MaxProfit {
    public int maxProfit(int[] A) {
        if (A.length == 1 || A.length == 0) {
            return 0;
        }
        int psum = 0;
        int ret = 0;
        int minPrice = A[0];

        for (int i = 1; i < A.length; i++) {
            minPrice = Math.min(minPrice, A[i]);
            psum = Math.max(0, A[i] - minPrice);

            ret = Math.max(psum, ret);
        }

        return ret;
    }

    public static void main(String[] args) {
        MaxProfit maxProfit = new MaxProfit();
        System.out.println(maxProfit.maxProfit(new int[]{23171, 21011, 21123, 21366, 21013, 21367}));

    }
}
