public class MaxDoubleSliceSum {
    public int solution(int[] A) {
        if(A.length == 3) return 0;

        int psum = A[1];
        int ret = A[1];
        int result = 0;

        int mid = A.length / 2;
        for (int i = 2; i < mid; i++) {
            psum = Math.max(psum + A[i], A[i]);
            ret = Math.max(psum, ret);
        }

        result += ret;

        psum = A[mid];
        ret = A[mid];
        for (int i = mid + 1; i < A.length - 1; i++) {
            psum = Math.max(psum + A[i], A[i]);
            ret = Math.max(psum, ret);
        }

        result += ret;

        return result;
    }

    public static void main(String[] args) {
        MaxDoubleSliceSum maxDoubleSliceSum = new MaxDoubleSliceSum();
        System.out.println(maxDoubleSliceSum.solution(new int[]{0, 10, -5, -2, 0}));
    }
}
