public class PermMissingElem2 {
    public int solution(int[] A) {
        int N = A.length;
        int perfectSum = (N + 2) * (N + 1) / 2;

        for (int i = 0; i < N; i++) {
            perfectSum -= A[i];
        }

        return perfectSum;
    }

    public static void main(String[] args) {
        PermMissingElem2 permMissingElem2 = new PermMissingElem2();
        System.out.println(permMissingElem2.solution(new int[]{2, 3, 1, 5, 4, 6, 7, 9}));
    }
}
