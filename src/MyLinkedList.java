public class MyLinkedList {
    int size = 0;

    private Node header;

    public MyLinkedList() {
        header = new Node(null);
    }

    class Node {
        Node nextNode;
        Object data;
        public Node(Object data) {
            this.data = data;
            this.nextNode = null;
        }
    }

    public void addFirst(Object data) {
        Node newNode = new Node(data);
        Node nextNode = header.nextNode;

        header.nextNode = newNode;
        newNode.nextNode = nextNode;

        size++;
    }

    public void add(int index, Object data) {
        if(index > size) throw new IllegalArgumentException();

        Node newNode = new Node(data);
        Node node = header.nextNode;
        for (int i = 0; i < index - 1; i++) {
            node = node.nextNode;
        }

        Node nextNode = node.nextNode;
        node.nextNode = newNode;
        newNode.nextNode = nextNode;

        size++;
    }

    public void removeFirst() {
        Node firstNode = header.nextNode;
        header.nextNode = firstNode.nextNode;
        size--;
    }

    public void remove(int index) {
        Node node = header.nextNode;
        for (int i = 0; i < index - 1; i++) {
            node = node.nextNode;
        }

        Node removeNode = node.nextNode;
        node.nextNode = removeNode.nextNode;
    }

    public Object get(int index) {
        return getNode(index).data;
    }

    private Node getNode(int index) {
        if(index > size) throw new IllegalArgumentException();

        Node node = header.nextNode;
        for (int i = 0; i < index; i++) {
            node = node.nextNode;
        }

        return node;
    }

}
