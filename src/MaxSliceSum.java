public class MaxSliceSum {
    public int solution(int[] A) {
        if (A.length == 1) return A[0];

        int maxSum = Integer.MIN_VALUE;
        for (int i = 0; i < A.length; i++) {
            int val = A[i];
            if (val > maxSum) maxSum = val;
            for (int j = i + 1; j < A.length; j++) {
                if (A[j] > maxSum) maxSum = A[j];
                val += A[j];
                if (val > maxSum) maxSum = val;
            }

        }
        return maxSum;
    }

    public int solution2(int[] A) {
        int sum = A[0];
        int result = A[0];

        for (int i = 1; i < A.length; i++) {
            int val = A[i];
            int max = sum + A[i];
            sum = Math.max(val, max);
            result = Math.max(sum, result);
        }

        return result;
    }

    public static void main(String[] args) {
        MaxSliceSum maxSliceSum = new MaxSliceSum();
//        ArrayUtil.printArr(20, -10, 10);

        System.out.println(maxSliceSum.solution2(new int[]{-3, 2, 6, 4, 0}));

    }
}
