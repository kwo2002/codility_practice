public class BinarySearch {
    public int search(int[] arr, int x) {
        int min = 0;
        int max = arr.length - 1;
        int mid = (min + max) / 2;

        while (arr[mid] != x && mid < max) {
            if (arr[mid] < x) {
                min = mid + 1;
            } else if (arr[mid] > x) {
                max = mid - 1;
            }

            mid = (min + max) / 2;
        }

        return mid;
    }

    public static void main(String[] args) {
        BinarySearch binarySearch = new BinarySearch();
        System.out.println(binarySearch.search(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9}, 9));
    }
}
