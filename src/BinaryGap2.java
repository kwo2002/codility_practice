public class BinaryGap2 {
    public int solution(int N) {
        String binary = Integer.toBinaryString(N);
        boolean oneFlag = false;
        int zeroCnt = 0;
        int maxGap = 0;

        for (int i = 0; i < binary.length(); i++) {
            char c = binary.charAt(i);
            if(c == '1') {
                if (oneFlag) {
                    if(zeroCnt > maxGap) {
                        maxGap = zeroCnt;
                    }
                    zeroCnt = 0;
                } else oneFlag = true;
            }

            if (oneFlag && c == '0') {
                zeroCnt++;
            }
        }

        return maxGap;
    }
    public static void main(String[] args) {
        BinaryGap2 binaryGap2 = new BinaryGap2();
        System.out.println(binaryGap2.solution(561892));
    }
}
