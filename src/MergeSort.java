public class MergeSort {
    public int[] mergeSort(int[] arr, int p, int r) {

        if (p < r) {
            int q = (p + r) / 2;

            // left
            mergeSort(arr, p, q);
            // right
            mergeSort(arr, q + 1, r);

            merge(arr, p, r);
        }
        return arr;
    }

    public void merge(int[] arr, int p, int r) {
        int q = (p + r) / 2;

        int[] lArr = new int[q - p + 1];
        int[] rArr = new int[r - q];

        int j = 0;
        for (int i = p; i <= q; i++) {
            lArr[j] = arr[i];
            j++;
        }

        j = 0;
        for (int i = q + 1; i <= r; i++) {
            rArr[j] = arr[i];
            j++;
        }

        int left = 0;
        int right = 0;
        for (int i = p; i <= r; i++) {
            if (!(left >= lArr.length) && !(right >= rArr.length)) {

                if (lArr[left] > rArr[right]) {
                    arr[i] = rArr[right];
                    right++;
                } else if (lArr[left] < rArr[right]) {
                    arr[i] = lArr[left];
                    left++;
                } else {
                    arr[i] = rArr[right];
                    right++;
                }
            } else {
                if (left >= lArr.length) {
                    arr[i] = rArr[right];
                    right++;
                } else if (right >= rArr.length) {
                    arr[i] = lArr[left];
                    left++;
                }
            }
        }
    }

    public static void main(String[] args) {
        MergeSort mergeSort = new MergeSort();
        int[] arr = {5, 4, 1, 6, 3, 323, 13, 94, 27, 5, 322, 66, 33, 11, 66, 36, 912, 40, 59, 12, 67, 77, 88};
        int[] ints = mergeSort.mergeSort(arr, 0, arr.length - 1);

        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
