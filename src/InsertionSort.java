public class InsertionSort {
    public void sort(int[] A) {
        for (int i = 1; i < A.length; i++) {
            int temp = A[i];
            int aux = i - 1;

            while ((aux >= 0) && (A[aux] > temp)) {
                A[aux + 1] = A[aux];
                aux--;
            }

            A[aux + 1] = temp;
        }

        for (int i : A) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        InsertionSort insertionSort = new InsertionSort();
        insertionSort.sort(new int[]{3, 2, 5, 4, 1});
    }
}
