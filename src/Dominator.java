import java.util.*;

public class Dominator {

    public int solution(int[] A) {
        if(A.length == 1) return 0;
        int dominator = findDominator(A);
        if(dominator == -1) return -1;

        List<Integer> dominatorIdxList = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            if(A[i] == dominator) dominatorIdxList.add(i);
        }

        return dominatorIdxList.get(0);
    }

    public int findDominator(int[] A) {
        int[] arr = Arrays.copyOf(A, A.length);

        Arrays.sort(arr);
        int half = arr.length / 2;
        int elementCnt = 1;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i - 1] == arr[i]) {
                elementCnt++;
                if (elementCnt > half) return arr[i];
            } else {
                elementCnt = 1;
            }
        }

        return -1;
    }


    public static void main(String[] args) {
        Dominator dominator = new Dominator();
        System.out.println(dominator.solution(new int[]{1, 2, 1}));
    }
}
