import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.stream.Collectors.joining;

public class ArrayUtil {
    public static int[] generateArr(int size, int startRange, int endRange) {
        int n = ThreadLocalRandom.current().nextInt(1, size);
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(startRange, endRange);
        }

        printArr(arr);

        return arr;
    }

    public static void printArr(int[] arr) {
        System.out.println("Generate int array: {" + Arrays.stream(arr).mapToObj(Integer::toString).collect(joining(", ")) + "}");
    }

    public static void printArr(int size, int startRange, int endRange) {
        int[] arr = generateArr(size, startRange, endRange);
        System.out.println("new int[]{" + Arrays.stream(arr).mapToObj(Integer::toString).collect(joining(", ")) + "}");
    }
}
