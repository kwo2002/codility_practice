public class BinaryGap {
    public int solution(int N) {
        int cnt = 0;
        int gap = 0;

        boolean openFlag = false;
        String binaryN = Integer.toBinaryString(N);
        for (int i = 0; i < binaryN.length(); i++) {
            char ch = binaryN.charAt(i);

            if (ch == '1') {
                if(openFlag) {
                    if(cnt > gap) {
                        gap = cnt;
                    }
                } else openFlag = true;
                cnt = 0;
            } else {
                if(openFlag) cnt++;
            }
        }
//101001000
        return gap;
    }

    public static void main(String[] args) {
        BinaryGap binaryGap = new BinaryGap();
        System.out.println(binaryGap.solution(1610612737));

    }
}
