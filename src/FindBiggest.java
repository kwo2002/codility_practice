public class FindBiggest {
    int[] find(int[] arr, int n) {
        if (arr == null || arr.length <= 0) {
            throw new IllegalArgumentException();
        }

        if (n <= 0 || n > arr.length) {
            throw new IllegalArgumentException();
        }

        for (int i = 1; i < arr.length; i++) {
            int idx = i - 1;
            int key = arr[i];

            while (idx >= 0 && arr[idx] > key) {
                arr[idx + 1] = arr[idx];
                idx--;
            }

            arr[idx + 1] = key;
        }

        return arr;
    }

    public static void main(String[] args) {
        FindBiggest findBiggest = new FindBiggest();
        int[] ints = findBiggest.find(new int[]{5, 4, 1, 6, 3, 323, 13, 94, 27, 5, 322, 66, 33, 11, 66, 77, 88}, 5);
//        int[] ints = findBiggest.find(new int[]{5, 4, 1, 6, 3}, 5);
        for (int anInt : ints) {
            System.out.println(anInt);
        }
    }
}
