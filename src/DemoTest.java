import java.util.Arrays;

public class DemoTest {
    public int solution(int[] A) {
        if (A.length == 1) {
            if(A[0] != 1) return 1;
            else return 2;
        }
        int[] arr = Arrays.stream(A).distinct().sorted().filter(i -> i > 0).toArray();
        if(arr.length == 0) return 1;

        int prev = arr[0];
        if(prev != 1) return 1;

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] != prev + 1) {
                return prev + 1;
            }
            prev = arr[i];
        }

        return arr[arr.length - 1] + 1;
    }

    public static void main(String[] args) {
        DemoTest demoTest = new DemoTest();
        System.out.println(demoTest.solution(new int[]{1, 3, 6, 4, 1, 2}));
    }
}
