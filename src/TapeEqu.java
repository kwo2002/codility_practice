public class TapeEqu {
    public int solution(int[] A) {
        int min = Integer.MAX_VALUE;
        int totalSum = 0;
        for (int i = 0; i < A.length; i++) {
            totalSum += A[i];
        }

        int sum = 0;
        for (int i = 0; i < A.length - 1; i++) {

            sum += A[i];
            int dif = Math.abs(sum - (totalSum - sum));
            if (dif < min) min = dif;
        }
        return min;
    }
    public static void main(String[] args) {
        TapeEqu tapeEqu = new TapeEqu();
        System.out.println(tapeEqu.solution(new int[]{3, 1, 2, 4, 3}));
    }
}
