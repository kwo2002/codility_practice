import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Brackets {
    public int solution(String S) {
        if(S.equals("")) return 1;
        Map<Character, Character> map = new HashMap<>();
        map.put('{', '}');
        map.put('(', ')');
        map.put('[', ']');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < S.length(); i++) {
            char str = S.charAt(i);
            if (map.containsKey(str)) {
                stack.push(map.get(str));
            }

            if (str == '}' || str == ')' || str == ']') {
                if (stack.empty()) {
                    return 0;
                } else {
                    if (str == stack.peek()) {
                        stack.pop();
                    }
                }
            }

        }

        return stack.empty() ? 1 : 0;
    }
    public static void main(String[] args) {
        Brackets brackets = new Brackets();
        int result = brackets.solution("())");

        System.out.println(result);

    }
}
