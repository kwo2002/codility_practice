public class FrogJump {
    public int solution(int X, int Y, int D) {
        if(X <= 0) throw new IllegalArgumentException();
        if(X > Y) throw new IllegalArgumentException();

        int distance = Y - X;
        int jumpCount = distance / D;
        if(distance % D >= 1) jumpCount++;
        return jumpCount;
    }

    public static void main(String[] args) {
        FrogJump frogJump = new FrogJump();
        System.out.println(frogJump.solution(10, 85, 30));
    }
}
