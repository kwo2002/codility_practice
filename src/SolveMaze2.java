public class SolveMaze2 {
    private static int maze[][] = {
            {1, 1, 1, 1, 0, 0, 0, 3, 0, 0},
            {1, 1, 1, 1, 0, 1, 1, 0, 1, 0},
            {1, 1, 1, 1, 0, 1, 1, 0, 1, 0},
            {0, 0, 0, 1, 0, 1, 1, 0, 1, 0},
            {0, 1, 0, 0, 0, 1, 1, 0, 1, 0},
            {0, 1, 1, 1, 1, 1, 1, 0, 1, 0},
            {0, 1, 1, 1, 0, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 0, 1, 1, 0, 1, 1},
            {1, 1, 1, 1, 0, 0, 0, 0, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    };

    public boolean escapeMaze(int y, int x) {
        if(maze[y][x] == 3) {
            return true;
        }

        maze[y][x] = 2;
        if (isAvailable(y - 1, x) && escapeMaze(y - 1, x)) {
            return true;
        }

        if (isAvailable(y + 1, x) && escapeMaze(y + 1, x)) {
            return true;
        }

        if (isAvailable(y, x + 1) && escapeMaze(y, x + 1)) {
            return true;
        }

        if (isAvailable(y, x - 1) && escapeMaze(y, x - 1)) {
            return true;
        }

        /*System.out.println();
        System.out.println();
        System.out.println();

        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                System.out.print(maze[i][j] + "  ");
            }
            System.out.println();
        }*/
        maze[y][x] = 0;

        return false;
    }

    private boolean isAvailable(int y, int x) {
        return y >= 0 && x >= 0 && y <= maze.length - 1 && x <= maze[0].length - 1 && maze[y][x] != 2 && (maze[y][x] == 0 || maze[y][x] == 3);
    }

    public static void main(String[] args) {
        SolveMaze2 solveMaze2 = new SolveMaze2();
        System.out.println(solveMaze2.escapeMaze(6, 4));



        /*for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze[i].length; j++) {
                System.out.print(maze[i][j] + "                  ");
            }
            System.out.println();
        }*/
    }
}
