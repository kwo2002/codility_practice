public class TapeEquilibrium {
    public int solution(int[] A) {
        if (A.length == 2) {
            return Math.abs(A[0] - A[1]);
        }

        int[] arr = new int[A.length];
        arr[0] = A[0];

        int totalSum = A[0];
        for (int i = 1; i < A.length; i++) {
            arr[i] = arr[i - 1] + A[i];
            totalSum += A[i];
        }

        int min = Integer.MAX_VALUE;
        for (int i = 0; i < A.length; i++) {
            int aType = arr[i];
            int bType = totalSum - arr[i];

            int dif = Math.abs(aType - bType);
            if (dif < min) {
                min = dif;
            }

        }

        return min;
    }

    public static void main(String[] args) {
        TapeEquilibrium tapeEquilibrium = new TapeEquilibrium();
        System.out.println(tapeEquilibrium.solution(new int[]{-10, -20, -30, -40, 100}));
    }
}
