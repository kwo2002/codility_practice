public class RockFestival {
    public int solution(int[] A) {
        int psum = A[0];
        int ret = A[0];

        for (int i = 1; i < A.length; i++) {
            psum = Math.max(psum + A[i], A[i]);
            ret = Math.max(psum, ret);
        }

        return ret;
    }

    public static void main(String[] args) {
        RockFestival rockFestival = new RockFestival();
        System.out.println(rockFestival.solution(new int[]{3, 0, -7, 4, 0}));
    }
}
