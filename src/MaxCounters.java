public class MaxCounters {
    public int[] solution(int N, int[] A) {

        int[] counterArr = new int[N];

        int max = 0;
        int prvMax = 0;

        for(int i = 0; i < A.length; i++) {
            int X = A[i];
            if(X >= 1 && X <= N) {
                counterArr[X - 1]++;

                if (counterArr[X - 1] + prvMax > max) {
                    max = counterArr[X - 1] + prvMax;
                }
            }

            if (X == N + 1) {
                counterArr = new int[N];
                prvMax = max;
            }
        }

        for (int i = 0; i < counterArr.length; i++) {
            counterArr[i] += prvMax;
        }

        return counterArr;
    }

    public static void main(String[] args) {
        MaxCounters maxCounters = new MaxCounters();
        int[] solution = maxCounters.solution(5, new int[]{3, 4, 4, 6, 1, 4, 4});
        for (int i : solution) {
            System.out.println(i);
        }
    }
}
