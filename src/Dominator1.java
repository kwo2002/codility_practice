public class Dominator1 {
    public int solution(int[] A) {
        if(A.length == 0) return -1;
        if(A.length == 1) return 0;

        int dominator = findDominator(A);

        int totalDominator = 0;

        for (int i = 0; i < A.length; i++) {
            if(A[i] == dominator) totalDominator++;
        }

        if(totalDominator <= A.length / 2) return -1;

        for (int i = 0; i < A.length; i++) {
            if(A[i] == dominator) return i;
        }

        return -1;
    }

    public int findDominator(int[] A) {
        int dominator = A[0];
        int cnt = 1;
        for (int i = 1; i < A.length; i++) {
            if(dominator == A[i]) cnt++;
            else cnt--;

            if (cnt == 0) {
                cnt = 1;
                dominator = A[i];
            }
        }

        return dominator;
    }

    public static void main(String[] args) {
        Dominator1 dominator1 = new Dominator1();
        System.out.println(dominator1.solution(new int[]{2, 1, 1, 3}));
    }
}
